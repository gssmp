#!/usr/bin/python 

import os, re, sys

sos_filename = "/tmp/gssmp_sos.txt"
pkg_filename = "/tmp/gssmp_pkgs.txt"

pkgln_re = re.compile( "(\S*):.*" )

skip_stage1 = False
skip_stage2 = False
skip_stage3 = False

if len( sys.argv ) > 1:
	stg = sys.argv[1]
	if stg == "--stage2":
		skip_stage1 = True
	elif stg == "--stage3":
		skip_stage1 = True
		skip_stage2 = True

if not skip_stage1:
	sys.stderr.write( "Stage 1: Find the shared objects the program uses\n" )
	os.system( "objdump -p src/gssmp | grep NEEDED | sort | uniq | awk '{print $2}' > %s" % sos_filename )

if not skip_stage2:
	sys.stderr.write(  "Stage 2: Find which packages provide each shared object\n" )
	sosfl = file( sos_filename, 'r' )
	pkgsfl = file( pkg_filename, 'w' )
	for so in sosfl:
		so = so.strip()
		( stdin, stdout ) = os.popen2( "dpkg -S %s" % so )
		for pkgln in stdout:
			print pkgln,
			m = pkgln_re.match( pkgln )
			if m:
				pkgsfl.write( "%s\n" % m.group( 1 ) )
				print m.group( 1 )
			else:
				print "Line not understood: %s" % pkgln
	sosfl.close()
	pkgsfl.close()

if not skip_stage3:
	sys.stderr.write( "Stage 3: Eliminate any packages which are depended on by any other package - "
		"no need to name them as dependencies\n" )
	
	# Read packages from file
	trimmed_pkgs = set()
	pkgsfl = file( pkg_filename, 'r' )
	for pkg in pkgsfl:
		pkg = pkg.strip()
		trimmed_pkgs.add( pkg )
	pkgsfl.close()
	
	sys.stderr.write( "\n" )
	for pkg in trimmed_pkgs:
		sys.stderr.write( "Pretrim %s\n" % pkg )
	
	sys.stderr.write( "\n" )
	pkgs = trimmed_pkgs.copy()
	for pkg in pkgs:
		#sys.stderr.write( "Checking things that depend on %s\n" % pkg )
		( stdin, stdout ) = os.popen2( "sudo apt-get --dry-run remove %s | grep Remv | awk '{print $2}'" % pkg )
		remvs = stdout.readlines()
		for remv in remvs:
			remv = remv.strip()
			if remv != pkg and remv in trimmed_pkgs:
				print "Trimming %20s since %20s depends on it" % ( pkg, remv )
				trimmed_pkgs.remove( pkg )
				try_again = True
				break

	#print
	#for t in pkgs_to_remove:
	#	sys.stderr.write( "Trimming %s\n" % t );
	
	
	
	#print
	#first_time = True
	#pkgs_to_remove = set()
	#while first_time or len( pkgs_to_remove ) > 0:
	#	pkgs_to_remove.clear()
	#	first_time = False
	#	pkgs = trimmed_pkgs.copy()
	#	for pkg in pkgs:
	#		( stdin, stdout ) = os.popen2( "dpkg -s %s | grep Depends:" % pkg )
	#		for depln in stdout:
	#			for possible_pkg in trimmed_pkgs:
	#				if depln.find( " %s " % possible_pkg ) != -1:
	#					pkgs_to_remove.add( possible_pkg )
	#					print "trim: %s" % possible_pkg
	#	trimmed_pkgs.difference_update( pkgs_to_remove )
	
	print
	for pkg in trimmed_pkgs:
		print pkg

