###
### RPM spec file for Gnome Simple Stateful Music Player
### 
### Adapted from jEdit's spec file http://www.jedit.org
###

Summary: A music player that doesn't interfere.
Name: gssmp
Provides: gssmp
Version: 1.2
Release: 1
License: GPL
Group: Applications/Multimedia/
Source0: %{name}-%{version}.tar.bz2
URL: http://gssmp.sourceforge.net/
Vendor: Andy Balaam <axis3x3@users.sourceforge.net>
Packager: Andy Balaam <axis3x3@users.sourceforge.net>
BuildArch: i386
BuildRoot: %{_builddir}/%{name}-root
Requires: libgnomeui, gstreamer
BuildRequires: libgnomeui-devel, gstreamer-devel

%description
GSSMP is a music player designed to work with Gnome.  It does not store a
database of all your music, but is designed to work with music files organised
into directories.  It is small, unobtrusive, and tries to do what you would
expect, instead of asking you questions.

It remembers which file was playing on exit, and continues that file from the
same place when you start again.  It remembers tracks you have been listening
to recently and displays them in the "Recent" menu.

%prep
%setup -n %{name}-%{version}

%build
./configure --prefix=/usr
make

%install
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
/usr/bin/gssmp
/usr/share/doc/gssmp/AUTHORS
/usr/share/doc/gssmp/COPYING
/usr/share/doc/gssmp/INSTALL
/usr/share/doc/gssmp/NEWS
/usr/share/doc/gssmp/README
/usr/share/doc/gssmp/TODO
/usr/share/doc/gssmp/html/contact.html
/usr/share/doc/gssmp/html/COPYING
/usr/share/doc/gssmp/html/developers.html
/usr/share/doc/gssmp/html/index.html
/usr/share/doc/gssmp/html/install-lastfm.html
/usr/share/doc/gssmp/html/install-mp3.html
/usr/share/doc/gssmp/html/install-prerequisites.html
/usr/share/doc/gssmp/html/install.html
/usr/share/doc/gssmp/html/screenshot-small.png
/usr/share/doc/gssmp/html/screenshot.png
/usr/share/doc/gssmp/html/screenshot2.png
/usr/share/doc/gssmp/html/screenshot3.png
/usr/share/doc/gssmp/html/screenshot4.png
/usr/share/doc/gssmp/html/screenshots.html
/usr/share/doc/gssmp/html/style.css
/usr/share/doc/gssmp/html/use.html
/usr/share/applications/gssmp.desktop
/usr/share/pixmaps/gssmp.png
/usr/share/man/man1/gssmp.1.gz

