## Process this file with automake to produce Makefile.in

SUBDIRS = src po

EXTRA_DIST = \
	autogen.sh \
	gssmp.glade \
	gssmp.gladep

APP_NAME=gssmp
TMP_DIR=/tmp/$(APP_NAME)

install-data-local: install/gssmp.1.gz
	@$(NORMAL_INSTALL)
	$(mkinstalldirs) $(DESTDIR)$(datadir)/pixmaps; \
	$(INSTALL_DATA) $(srcdir)/pixmaps/gssmp.png $(DESTDIR)$(datadir)/pixmaps;
	$(mkinstalldirs) $(DESTDIR)$(datadir)/doc/$(PACKAGE); \
	for docfile in AUTHORS COPYING INSTALL NEWS README TODO; do \
	  if test -f $$docfile; then \
	    $(INSTALL_DATA) $$docfile $(DESTDIR)$(datadir)/doc/$(PACKAGE); \
	  fi \
	done
	$(mkinstalldirs) $(DESTDIR)$(datadir)/doc/$(PACKAGE)/html; \
	for docfile in www/COPYING www/*.html www/*.css www/*.png; do \
	  $(INSTALL_DATA) $$docfile $(DESTDIR)$(datadir)/doc/$(PACKAGE)/html; \
	done
	$(mkinstalldirs) $(DESTDIR)$(datadir)/man/man1; \
	$(INSTALL_DATA) install/gssmp.1.gz $(DESTDIR)$(datadir)/man/man1;
	$(mkinstalldirs) $(DESTDIR)$(datadir)/applications; \
	$(INSTALL_DATA) install/gssmp.desktop $(DESTDIR)$(datadir)/applications;

dist-hook:
	if test -d pixmaps; then \
	  mkdir $(distdir)/pixmaps; \
	  for pixmap in pixmaps/*; do \
	    if test -f $$pixmap; then \
	      cp -p $$pixmap $(distdir)/pixmaps; \
	    fi \
	  done \
	fi

install/gssmp.1.gz: install/gssmp.1
	gzip --stdout install/gssmp.1 > install/gssmp.1.gz

pkg-src:
	mkdir -p pkg
	rm -f pkg/$(APP_NAME)-*.tar.bz2
	- rm -r $(TMP_DIR)
	mkdir $(TMP_DIR)
	git archive --format=tar --prefix=$(APP_NAME)-$(VERSION)/ master > pkg/$(APP_NAME)-$(VERSION).tar
	tar -rf pkg/$(APP_NAME)-$(VERSION).tar --transform \
	"s%.*%$(APP_NAME)-$(VERSION)/\\0%" configure install-sh Makefile.in \
	config.h.in mkinstalldirs autom4te.cache missing stamp-h.in \
	depcomp src/Makefile.in
	bzip2 pkg/$(APP_NAME)-$(VERSION).tar

pkg-bin: pkg-src install/gssmp.1.gz all
	- rm -r $(TMP_DIR)
	mkdir -p $(TMP_DIR)/$(APP_NAME)-$(VERSION)-bin/
	tar --directory $(TMP_DIR)/ -xjf pkg/$(APP_NAME)-$(VERSION).tar.bz2
	cd $(TMP_DIR)/$(APP_NAME)-$(VERSION)/; \
		./configure --prefix=$(TMP_DIR)/$(APP_NAME)-$(VERSION)-bin
	make --directory $(TMP_DIR)/$(APP_NAME)-$(VERSION)
	make --directory $(TMP_DIR)/$(APP_NAME)-$(VERSION) install
	tar --directory $(TMP_DIR)/ -cjf pkg/$(APP_NAME)-$(VERSION)-bin.tar.bz2 \
		$(APP_NAME)-$(VERSION)-bin
	rm -r $(TMP_DIR)

# Create a deb package
pkg-deb: pkg-src
	- rm -r $(TMP_DIR)
	mkdir -p $(TMP_DIR)
	cp pkg/$(APP_NAME)-$(VERSION).tar.bz2 $(TMP_DIR)/
	tar --directory $(TMP_DIR)/ \
		-xjf $(TMP_DIR)/$(APP_NAME)-$(VERSION).tar.bz2
	cd $(TMP_DIR)/$(APP_NAME)-$(VERSION)/; \
		echo | dh_make --single --copyright gpl -e axis3x3@users.sf.net -f \
			../$(APP_NAME)-$(VERSION).tar.bz2
	cp install/deb/changelog install/deb/control \
		install/deb/copyright $(TMP_DIR)/$(APP_NAME)-$(VERSION)/debian/
	cd $(TMP_DIR)/$(APP_NAME)-$(VERSION)/; \
		rm debian/README.Debian debian/*.ex debian/*.EX; \
		./configure; \
		dpkg-buildpackage -rfakeroot; \
		mv ../*.deb $(PWD)/pkg/
	rm -r $(TMP_DIR);

# Create an RPM package
pkg-rpm: pkg-src
	mkdir -p $(HOME)/.rpm/RPMS/i386
	mkdir -p $(HOME)/.rpm/SRPMS
	mkdir -p $(HOME)/.rpm/BUILD
	mkdir -p $(HOME)/.rpm/SOURCES
	mkdir -p $(HOME)/.rpm/tmp
	- rm -r $(HOME)/.rpm/BUILD/$(APP_NAME)-root
	- rm -r $(HOME)/.rpm/RPMS/$(APP_NAME)-*
	- rm -r $(HOME)/.rpm/SRPMS/$(APP_NAME)-*
	- rm -r $(HOME)/.rpm/SOURCES/$(APP_NAME)-*
	cp pkg/$(APP_NAME)-$(VERSION).tar.bz2 $(HOME)/.rpm/SOURCES/
	rpmbuild --quiet --nodeps --define="_topdir $(HOME)/.rpm" -ba install/rpm/gssmp.spec
	mv $(HOME)/.rpm/RPMS/i386/$(APP_NAME)-$(VERSION)-*.i386.rpm pkg/

# Make all binary packages, ready to upload to sourceforge
pkg-all: pkg-bin pkg-deb pkg-rpm
	
# Push the current source code to my git repository
git-upload:
	- rm -r $(TMP_DIR)
	git-repack -d
	git clone --bare -l . $(TMP_DIR)/$(APP_NAME).git
	rsync -r --delete $(TMP_DIR)/$(APP_NAME).git/ \
		artific2@artificialworlds.net:public_html/gssmp/git/gssmp.git/
	rm -r $(TMP_DIR);


