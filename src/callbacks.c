/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "gssmp_config.h"
#include "gssmp_ui.h"
#include "gssmp_test.h"

gboolean
on_window1_delete_event                (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  gssmp_ui_quit ();
  return FALSE;
}

void
on_button4_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  gssmp_ui_next_clicked ();
}


void
on_button1_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  gssmp_ui_previous_clicked ();
}


void
on_button2_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  gssmp_ui_play_clicked ();
}


void
on_button3_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  gssmp_ui_pause_clicked ();
}

gboolean
on_hscale1_change_value                (GtkRange        *range,
                                        GtkScrollType    scroll,
                                        gdouble          value,
                                        gpointer         user_data)
{
  gssmp_ui_slider_changed (value);
  return FALSE;
}


void
on_open_directory1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gssmp_ui_choose_directory_clicked ();
}


void
on_open_file1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gssmp_ui_choose_file_clicked ();
}

void
on_recent1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
}

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gssmp_ui_quit ();
}


void
on_repeat_all1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  config_repeat_style = repeat_all;
}


void
on_repeat_one1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  config_repeat_style = repeat_one;
}
                                           

void
on_shuffle1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  config_repeat_style = shuffle;
}


void
on_no_repeat1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  config_repeat_style = no_repeat;
}

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gssmp_ui_about ();
}


gboolean
on_vscale1_change_value                (GtkRange        *range,
                                        GtkScrollType    scroll,
                                        gdouble          value,
                                        gpointer         user_data)
{
  gssmp_ui_volume_change (value);
  return FALSE;
}


void
on_how_to_use1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gssmp_ui_launch_help ();
}


void
on_lastfm1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gssmp_ui_lastfm ();
}


void
on_checkbutton1_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  gssmp_ui_lastfm_statechange ();
}

void
on_entry1_changed                      (GtkEditable     *editable,
                                        gpointer         user_data)
{
  gssmp_ui_lastfm_exechanged ();
}

gboolean
on_lastfmwindow_delete_event           (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  gssmp_ui_lastfm_close ();
  return TRUE;
}

void
on_button5_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  gssmp_ui_lastfm_close ();
}

void
on_about_response                      (GtkDialog       *dialog,
                                        gint             arg1,
                                        gpointer         user_data)
{
  gtk_widget_hide( GTK_WIDGET(dialog) );
}
