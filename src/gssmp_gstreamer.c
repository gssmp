/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include "gssmp_gstreamer.h"

#include <gst/gst.h>

#define PROGRESS_TIMEOUT_LENGTH 1000

GstElement *play;

gboolean bus_callback (GstBus *bus, GstMessage *message, gpointer data);
void internal_tag_callback ( const GstTagList *tag_list, const gchar* tag );
void internal_tag_callback_uint (const GstTagList *tag_list, const gchar* tag);
gboolean internal_progress_callback ();
gboolean internal_duration_callback ();
void test_dump_tag (const GstTagList *list, const gchar *tag, gpointer
  user_data);

void (*external_duration_callback)(gint secs);
void (*external_progress_callback)(gint secs);
void (*external_tag_callback)(const gchar* tag, const gchar* value);
void (*external_eos_callback)();
void (*external_error_callback)(const gchar* message);

gint stored_start_offset;
gboolean something_playing;

void gssmp_gstreamer_init (int argc, char *argv[])
{
  GstElement* fakesink;
  
  gst_init (&argc, &argv);
  play     = gst_element_factory_make ("playbin",  "play");
  fakesink = gst_element_factory_make ("fakesink", "sink");
  
  // Suppress video output
  g_object_set (play, "video-sink", fakesink, NULL);
  
  something_playing = FALSE;
  
  if (play)
  {
    GstBus* bus;
    bus = gst_pipeline_get_bus (GST_PIPELINE (play));
    if (bus)
    {
      gst_bus_add_watch (bus, bus_callback, NULL);
    }
    else
    {
      fprintf (stderr, _("Unable to make the gstreamer pipeline bus.\n"));
    }
  }
  else
  {
    fprintf (stderr, _("Unable to make the gstreamer playbin.\n"));
  }
  
  g_timeout_add (PROGRESS_TIMEOUT_LENGTH, internal_progress_callback, NULL);
  
  external_duration_callback = NULL;
  external_progress_callback = NULL;
  external_tag_callback      = NULL;
  external_eos_callback      = NULL;
  external_error_callback    = NULL;
}

void gssmp_gstreamer_quit ()
{
  gst_element_set_state (play, GST_STATE_NULL);
  gst_object_unref (GST_OBJECT (play));
}

void gssmp_gstreamer_play (const gchar* filename, const gint start_offset,
  void (*duration_callback)(gint secs),
  void (*progress_callback)(gint secs),
  void (*tag_callback)(const gchar* tag, const gchar* value),
  void (*eos_callback)(), void (*error_callback)(const gchar* message))
{
  gchar* uri;
  
  stored_start_offset = start_offset;
  
  external_duration_callback = duration_callback;
  external_progress_callback = progress_callback;
  external_tag_callback      = tag_callback;
  external_eos_callback      = eos_callback;
  external_error_callback    = error_callback;
  
  gst_element_set_state (play, GST_STATE_READY);
  
  uri = g_strconcat ("file://", filename, NULL);
  
  g_object_set (G_OBJECT (play), "uri", uri, NULL);
  something_playing = TRUE;
  
  gst_element_set_state (play, GST_STATE_PLAYING);
  
  g_free (uri);
}

void gssmp_gstreamer_unpause ()
{
  if (something_playing)
  {
    gst_element_set_state (play, GST_STATE_PLAYING);
  }
}

void gssmp_gstreamer_pause ()
{
  if (something_playing)
  {
    gst_element_set_state (play, GST_STATE_PAUSED);
  }
}

void gssmp_gstreamer_seek (gint secs)
{
  gint64 nanos;
  nanos = secs * GST_SECOND;
  
  if (!gst_element_seek(GST_ELEMENT(play), 1.0, GST_FORMAT_TIME,
    GST_SEEK_FLAG_FLUSH,
    GST_SEEK_TYPE_SET, nanos,
    GST_SEEK_TYPE_NONE, GST_CLOCK_TIME_NONE ))
  {
    fprintf (stderr, _("Seek failed!\n"));
  }
}

void gssmp_gstreamer_set_volume (gdouble value)
{
  g_object_set( G_OBJECT(play), "volume", value, NULL );
}

// --------------------------------

gboolean bus_callback (GstBus *bus, GstMessage *message, gpointer data)
{
  switch( message->type )
  {
    case GST_MESSAGE_TAG:
    {
      const gchar *message_type_name;
      GstTagList *tag_list;
      
      tag_list = NULL;
      
      message_type_name = gst_message_type_get_name (message->type);
      
      gst_message_parse_tag (message, &tag_list);
      
      internal_tag_callback (tag_list, "title");
      internal_tag_callback (tag_list, "artist");
      internal_tag_callback (tag_list, "album");
      internal_tag_callback_uint (tag_list, "track-number");
      internal_tag_callback_uint (tag_list, "track-count");
      
      // For debugging tags:
      //gst_tag_list_foreach (tag_list, (GstTagForeachFunc)test_dump_tag, NULL);
      
      g_free (tag_list);
      break;
    }
    case GST_MESSAGE_NEW_CLOCK:
    {
      internal_duration_callback ();
      if ((stored_start_offset > 0))
      {
        gint64 real_offset;
        real_offset = stored_start_offset * GST_SECOND;
        stored_start_offset = 0;
        
        if (gst_element_seek(GST_ELEMENT(play), 1.0, GST_FORMAT_TIME,
          GST_SEEK_FLAG_FLUSH,
          GST_SEEK_TYPE_SET, real_offset,
          GST_SEEK_TYPE_NONE, GST_CLOCK_TIME_NONE ))
        {
          internal_progress_callback ();
        }
        else
        {
          fprintf (stderr, _("Seek failed!\n"));
        }
      }
      break;
    }
    case GST_MESSAGE_CLOCK_PROVIDE:
    {
      internal_duration_callback ();
      break;
    }
    case GST_MESSAGE_DURATION:
    {
      internal_duration_callback ();
      break;
    }
    case GST_MESSAGE_EOS:
    {
      if (external_eos_callback)
      {
        external_eos_callback ();
      }
      break;
    }
    case GST_MESSAGE_ERROR:
    {
      GError* gerror;
      
      gst_message_parse_error (message, &gerror, NULL);
      fprintf (stderr, _("gstreamer error: %s\n"), gerror->message );
      
      gst_element_set_state (play, GST_STATE_NULL);
      something_playing = FALSE;
      
      external_error_callback (gerror->message);
      
      g_error_free (gerror);
      break;
    }
    case GST_MESSAGE_WARNING:
    {
      GError* gerror;
      
      gst_message_parse_warning (message, &gerror, NULL);
      fprintf ( stderr, _("gstreamer warning: %s\n"), gerror->message );
      
      g_error_free (gerror);
      break;
    }
    case GST_MESSAGE_STATE_CHANGED:
    case GST_MESSAGE_STATE_DIRTY:
    case GST_MESSAGE_CLOCK_LOST:
    {
      // Ignore - not particularly interesting
      break;
    }
    default:
    {
      fprintf( stderr, _("Ignored a '%s' message from gstreamer.\n"),
        GST_MESSAGE_TYPE_NAME(message) );
      break;
    }
  }
  
  return TRUE;
}

void internal_tag_callback_uint (const GstTagList *tag_list, const gchar* tag)
{
  uint value;
  
  if (gst_tag_list_get_uint (tag_list, tag, &value) && external_tag_callback)
  {
    char svalue[50];
    
    snprintf (svalue, 50, "%d", value);
    external_tag_callback (tag, svalue);
  }
}

void internal_tag_callback (const GstTagList *tag_list, const gchar* tag)
{
  gchar* value;
  
  value = NULL;
  if (gst_tag_list_get_string (tag_list, tag, &value) && external_tag_callback)
  {
    external_tag_callback (tag, value);
    
    g_free (value);
  }
}

gboolean internal_progress_callback ()
{
  GstState stt;
  gst_element_get_state (play, &stt, NULL, GST_CLOCK_TIME_NONE);
  
  if (stt == GST_STATE_PLAYING)
  {
    gint64 nanos;
    gint secs;
    static GstFormat format = GST_FORMAT_TIME;
    
    if (gst_element_query_position (play, &format, &nanos )
      && external_progress_callback)
    {
      secs = nanos / GST_SECOND;
      external_progress_callback (secs);
    }
    else
    {
      fprintf (stderr, "Failed to query position\n" );
    }
  }
  
  return TRUE;
}

gboolean internal_duration_callback ()
{
  gint64 nanos;
  gint secs;
  static GstFormat format = GST_FORMAT_TIME;
	
  if (gst_element_query_duration (play, &format, &nanos )
    && external_duration_callback)
  {
    secs = nanos / GST_SECOND;
    external_duration_callback (secs);
  }
  /* Fails quite often - presumably nothing to worry about
  else
  {
    fprintf (stderr, "Failed to query duration\n" );
  }*/
  
  return TRUE;
}

// -----------------------------------------

void test_dump_tag (const GstTagList *list, const gchar *tag, gpointer
  user_data)
{
  GType type;
  
  type = gst_tag_get_type (tag);
  
  switch (type)
  {
    case G_TYPE_INT:
    {
      int value;
      if (gst_tag_list_get_int (list, tag, &value))
      {
        printf ("tag: %s -> %d (int)\n", tag, value);
      }
      else
      {
        printf ("tag: %s -> NOTHING INT\n", tag);
      }
      break;
    }
    case G_TYPE_UINT:
    {
      uint value;
      if (gst_tag_list_get_uint (list, tag, &value))
      {
        printf ("tag: %s -> %d (uint)\n", tag, value);
      }
      else
      {
        printf ("tag: %s -> NOTHING INT\n", tag);
      }
      break;
    }
    case G_TYPE_LONG:
    {
      long value;
      if (gst_tag_list_get_long (list, tag, &value))
      {
        printf ("tag: %s -> %ld (long)\n", tag, value);
      }
      else
      {
        printf ("tag: %s -> NOTHING INT\n", tag);
      }
      break;
    }
    case G_TYPE_ULONG:
    {
      ulong value;
      if (gst_tag_list_get_ulong (list, tag, &value))
      {
        printf ("tag: %s -> %ld (ulong)\n", tag, value);
      }
      else
      {
        printf ("tag: %s -> NOTHING INT\n", tag);
      }
      break;
    }
    case G_TYPE_STRING:
    {
      gchar* value;
      if (gst_tag_list_get_string (list, tag, &value))
      {
        printf ("tag: %s -> %s\n", tag, value);
        g_free (value);
      }
      else
      {
        printf ("tag: %s -> NOTHING STRING\n", tag);
      }
      break;
    }
    default:
    {
      printf ("tag: %s -> UNKNOWN\n", tag);
      break;
    }
  }
}


