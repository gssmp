/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include <gnome.h>

void gssmp_ui_init (int argc, char *argv[]);
void gssmp_ui_quit ();
void gssmp_ui_about ();
void gssmp_ui_choose_directory_clicked ();
void gssmp_ui_choose_file_clicked ();
void gssmp_ui_eos_callback ();
void gssmp_ui_next_clicked ();
void gssmp_ui_previous_clicked ();

void gssmp_ui_directory_changed ();
void gssmp_ui_play_file (gchar* filename, gboolean change_dir,
  gboolean new_filename, gint start_offset, gboolean skip_to_next_if_fail);
void gssmp_ui_play_clicked ();
void gssmp_ui_pause_clicked ();
void gssmp_ui_slider_changed (gdouble value);
void gssmp_ui_volume_change (gdouble value);

void gssmp_ui_progress_callback (gint secs);
void gssmp_ui_duration_callback (gint secs);
void gssmp_ui_tag_callback (const gchar* tag, const gchar* value);
void gssmp_ui_error_callback (const gchar* msg);

void gssmp_ui_clear_track_info ();

void gssmp_ui_display_status_message (guint context_id, const gchar* msg,
  const gchar* sub);

void gssmp_ui_launch_help ();

void gssmp_ui_lastfm ();
void gssmp_ui_lastfm_statechange ();
void gssmp_ui_lastfm_exechanged ();
void gssmp_ui_lastfm_close ();
  
// ------------------------------------

void gssmp_ui_test_set_dir ();

