/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include "gssmp_config.h"

#include <gnome.h>
#include <gconf/gconf-client.h>

#define GCONF_KEY_ROOT      "/apps/gssmp/"
#define GCONF_KEY_BOOKMARKS "bookmarks/"

typedef struct
{
  const gchar* album;
  const gchar* filename;
  const gchar* directory;
} TrackDetails;


  // The head of the queue is the most recent bookmark
GQueue*   config_bookmarks;

gchar* current_directory;
gchar* current_filename;
gint   current_time;
gint   current_length;

gint             config_num_bookmarks;
gboolean         config_paused;
enum repeatstyle config_repeat_style;

gboolean config_lastfm_active;
gchar*   config_lastfm_exe;

void bookmarks_shorten_queue (guint length);
void bookmark_free_members (GssmpBookmark* bmk);
gint compare_bookmark_same_album_name (GssmpBookmark* bmk, TrackDetails* af);

void config_set (GConfClient *client, const gchar* key, const gchar* value);
void config_set_int (GConfClient *client, const gchar* key, const gint value);
void config_set_repeatstyle (GConfClient *client, const gchar* key,
  const enum repeatstyle value);
void config_set_bool (GConfClient *client, const gchar* key,
  const gboolean value);

void config_get (GConfClient *client, const gchar* key, gchar** value);
void config_get_int (GConfClient *client, const gchar* key, gint* value);
void config_get_bool (GConfClient *client, const gchar* key, gboolean* value);
void config_get_repeatstyle (GConfClient *client, const gchar* key,
  enum repeatstyle* value);

void bookmarks_load (GConfClient *client);
void bookmarks_save (GConfClient *client);

void gssmp_config_init ()
{
  GConfClient *client;
  
  client = gconf_client_get_default ();
  
  config_get_int         (client, "num_bookmarks", &config_num_bookmarks);
  config_get_bool        (client, "paused",        &config_paused);
  config_get_repeatstyle (client, "repeat_style",  &config_repeat_style);
  
  config_get_bool        (client, "lastfm/active", &config_lastfm_active);
  config_get             (client, "lastfm/exe",    &config_lastfm_exe);
  if (!config_lastfm_exe)
  {
    config_lastfm_exe = g_strdup( "lastfmsubmit" );
  }
  
  current_length = 0;
  
  bookmarks_load (client);
}

void gssmp_config_quit ()
{
  // Save config
  GConfClient *client;
  
  client = gconf_client_get_default ();
  
  config_set_int         (client, "num_bookmarks", config_num_bookmarks);
  config_set_bool        (client, "paused",        config_paused);
  config_set_repeatstyle (client, "repeat_style",  config_repeat_style);
  
  config_set_bool        (client, "lastfm/active", config_lastfm_active);
  config_set             (client, "lastfm/exe",    config_lastfm_exe);
  
  bookmarks_save (client);
  
  // Clean up memory
  bookmarks_shorten_queue (0);
  
  g_queue_free (config_bookmarks);
  g_free (current_directory);
  g_free (current_filename);
}

void gssmp_config_set_directory (const gchar* cfg_dir)
{
  int len;
  
  len = strlen (cfg_dir);
  
  if (cfg_dir[len-1] == G_DIR_SEPARATOR)
  {
    --len;
  }
  
  current_directory = g_strndup (cfg_dir, len);
}

void gssmp_config_add_bookmark (const gchar* track, const gchar* artist,
  const gchar* album, const gchar* directory, const gchar* filename,
  gint time, gint length)
{
  GssmpBookmark* bmk;
  GList* foundlist;
  
  if (config_bookmarks)
  {
    TrackDetails *af;
    
    af = g_slice_new (TrackDetails);
    af->album = album;
    af->filename = filename;
    af->directory = directory;
    
    foundlist = g_queue_find_custom (config_bookmarks, af,
      (GCompareFunc)compare_bookmark_same_album_name);
    
    g_slice_free (TrackDetails, af);
  }
  else
  {
    foundlist = NULL;
  }
  
  if (foundlist)
  {
    bmk = (GssmpBookmark*)(foundlist->data);
    bookmark_free_members (bmk);
    g_queue_remove (config_bookmarks, bmk);
      // Remove from queue so we can re-add at the front
  }
  else
  {
    bmk = g_slice_alloc (sizeof (GssmpBookmark));
  }
  
  bmk->track     = g_strdup (track);
  bmk->artist    = g_strdup (artist);
  bmk->album     = g_strdup (album);
  bmk->directory = g_strdup (directory);
  bmk->filename  = g_strdup (filename);
  bmk->time      = time;
  bmk->length    = length;
  
  g_queue_push_head (config_bookmarks, bmk);
  
  bookmarks_shorten_queue (config_num_bookmarks);
}

GssmpBookmark* gssmp_config_get_bookmark (guint number)
{
  return g_queue_peek_nth (config_bookmarks, number);
}

guint gssmp_config_bookmarks_length ()
{
  return g_queue_get_length (config_bookmarks);
}

// -------------------------

void bookmarks_shorten_queue (guint length)
{
  while (g_queue_get_length (config_bookmarks) > length)
  {
    GssmpBookmark* deleted_bmk;
    deleted_bmk = g_queue_pop_tail (config_bookmarks);
    
    bookmark_free_members (deleted_bmk);
    g_slice_free( GssmpBookmark, deleted_bmk );
  }
}

void bookmark_free_members (GssmpBookmark* bmk)
{
  g_free (bmk->track);
  g_free (bmk->artist);
  g_free (bmk->album);
  g_free (bmk->directory);
  g_free (bmk->filename);
}

gint compare_bookmark_same_album_name (GssmpBookmark* bmk, TrackDetails* af)
{
  if (strcmp (bmk->album, af->album) == 0)
  {
    // If the album names are the same, they match.
    return 0;
  }
  else if (strcmp (bmk->directory, af->directory) == 0)
  {
    // If the directories are the same, they match.
    return 0;
  }
  else
  {
    // Obviously, if it's the same file, they match.
    return strcmp (bmk->filename, af->filename);
  }
}

void config_set (GConfClient *client, const gchar* key, const gchar* value)
{
  if (value)
  {
    gchar *full_key;
    full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
    gconf_client_set_string (client, full_key, value, NULL);
    g_free (full_key);
  }
}

void config_set_int (GConfClient *client, const gchar* key, const gint value)
{
  gchar *full_key;
  full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
  gconf_client_set_int (client, full_key, value, NULL);
  g_free (full_key);
}

void config_set_repeatstyle (GConfClient *client, const gchar* key,
  const enum repeatstyle value)
{
  gchar *full_key;
  full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
  gconf_client_set_int (client, full_key, (gint)value, NULL);
  g_free (full_key);
}

void config_set_bool (GConfClient *client, const gchar* key,
  const gboolean value)
{
  gchar *full_key;
  full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
  gconf_client_set_bool (client, full_key, value, NULL);
  g_free (full_key);
}

void config_get (GConfClient *client, const gchar* key, gchar** value)
{
  gchar *full_key;
  full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
  *value = gconf_client_get_string (client, full_key, NULL);
  g_free (full_key);
}

void config_get_int (GConfClient *client, const gchar* key, gint* value)
{
  gchar *full_key;
  full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
  *value = gconf_client_get_int (client, full_key, NULL);
  g_free (full_key);
}

void config_get_repeatstyle (GConfClient *client, const gchar* key,
  enum repeatstyle* value)
{
  gchar *full_key;
  full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
  *value = (enum repeatstyle)(gconf_client_get_int (client, full_key, NULL));
  g_free (full_key);
}

void config_get_bool (GConfClient *client, const gchar* key, gboolean* value)
{
  gchar *full_key;
  full_key = g_strconcat (GCONF_KEY_ROOT, key, NULL);
  *value = gconf_client_get_bool (client, full_key, NULL);
  g_free (full_key);
}

void bookmarks_load (GConfClient *client)
{
  gboolean more_bmks;
  uint bmk_counter;
  
  if (config_num_bookmarks == 0)
  {
    config_num_bookmarks = 10;
  }
  
  more_bmks = TRUE;
  config_bookmarks = g_queue_new ();
  
  for (bmk_counter = 0;
       bmk_counter < config_num_bookmarks && more_bmks;
       ++bmk_counter)
  {
    gchar* bmk_track;
    gchar* key_base;
    gchar* key_track;
    
    key_base = g_strdup_printf ("%s%s%d", GCONF_KEY_ROOT,
      GCONF_KEY_BOOKMARKS, bmk_counter);
    
    key_track = g_strconcat (key_base, "/track", NULL);
    
    bmk_track = gconf_client_get_string (client, key_track, NULL);
    
    if (bmk_track)
    {
      GssmpBookmark* bmk;
      gchar* key_artist;
      gchar* key_album;
      gchar* key_directory;
      gchar* key_filename;
      gchar* key_time;
      gchar* key_length;
      gchar* bmk_artist;
      gchar* bmk_album;
      gchar* bmk_directory;
      gchar* bmk_filename;
      gint bmk_time;
      gint bmk_length;
      
      key_artist    = g_strconcat (key_base, "/artist",    NULL);
      key_album     = g_strconcat (key_base, "/album",     NULL);
      key_directory = g_strconcat (key_base, "/directory", NULL);
      key_filename  = g_strconcat (key_base, "/filename",  NULL);
      key_time      = g_strconcat (key_base, "/time",      NULL);
      key_length    = g_strconcat (key_base, "/length",    NULL);
      
      bmk_artist    = gconf_client_get_string (client, key_artist,    NULL);
      bmk_album     = gconf_client_get_string (client, key_album,     NULL);
      bmk_directory = gconf_client_get_string (client, key_directory, NULL);
      bmk_filename  = gconf_client_get_string (client, key_filename,  NULL);
      bmk_time      = gconf_client_get_int    (client, key_time,      NULL);
      bmk_length    = gconf_client_get_int    (client, key_length,    NULL);
      
      bmk = g_slice_alloc (sizeof (GssmpBookmark));
      
      bmk->track     = bmk_track;
      bmk->artist    = bmk_artist;
      bmk->album     = bmk_album;
      bmk->directory = bmk_directory;
      bmk->filename  = bmk_filename;
      bmk->time      = bmk_time;
      bmk->length    = bmk_length;
      
      g_queue_push_tail (config_bookmarks, bmk);
      
      g_free (key_artist);
      g_free (key_album);
      g_free (key_directory);
      g_free (key_filename);
      g_free (key_time);
      g_free (key_length);
    }
    else
    {
      more_bmks = FALSE;
    }
    
    g_free (key_track);
    g_free (key_base);
  }
}

void bookmarks_save (GConfClient *client)
{
  uint bmk_counter;
  GList *queue_item;
  
  for (bmk_counter = 0, queue_item = g_queue_peek_head_link (config_bookmarks);
       bmk_counter < config_num_bookmarks && queue_item;
       ++bmk_counter, queue_item = g_list_next (queue_item))
  {
    gchar *key_base;
    gchar *key_track;
    gchar *key_artist;
    gchar *key_album;
    gchar *key_directory;
    gchar *key_filename;
    gchar *key_time;
    gchar *key_length;
    GssmpBookmark *bmk;
    
    bmk = (GssmpBookmark*)(queue_item->data);
    
    key_base = g_strdup_printf ("%s%s%d", GCONF_KEY_ROOT,
      GCONF_KEY_BOOKMARKS, bmk_counter);
    
    key_track     = g_strconcat (key_base, "/track",     NULL);
    key_artist    = g_strconcat (key_base, "/artist",    NULL);
    key_album     = g_strconcat (key_base, "/album",     NULL);
    key_directory = g_strconcat (key_base, "/directory", NULL);
    key_filename  = g_strconcat (key_base, "/filename",  NULL);
    key_time      = g_strconcat (key_base, "/time",      NULL);
    key_length    = g_strconcat (key_base, "/length",    NULL);
    
    gconf_client_set_string (client, key_track,     bmk->track,     NULL);
    gconf_client_set_string (client, key_artist,    bmk->artist,    NULL);
    gconf_client_set_string (client, key_album,     bmk->album,     NULL);
    gconf_client_set_string (client, key_directory, bmk->directory, NULL);
    gconf_client_set_string (client, key_filename,  bmk->filename,  NULL);
    gconf_client_set_int    (client, key_time,      bmk->time,      NULL);
    gconf_client_set_int    (client, key_length,    bmk->length,    NULL);
    
    g_free (key_base);
    g_free (key_track);
    g_free (key_artist);
    g_free (key_album);
    g_free (key_directory);
    g_free (key_filename);
    g_free (key_time);
    g_free (key_length);
  }
}

void gssmp_config_bookmarks_foreach (
  void (*callback) (GssmpBookmark*, gpointer),
  gpointer user_data)
{
  g_queue_foreach (config_bookmarks, (GFunc)callback, user_data);
}


