#include <gnome.h>

/**
 * Loop through the queue, calling g_free on each piece of data, and then
 * call g_queue_free on the queue.
 */
void gssmp_util_free_queue_and_data (GQueue *queue);


void gssmp_util_free_list_and_data (GList *list);

