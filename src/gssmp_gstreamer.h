/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include <gnome.h>

void gssmp_gstreamer_init (int argc, char *argv[]);
void gssmp_gstreamer_quit ();

void gssmp_gstreamer_play (const gchar* filename, const gint start_offset,
  void (*duration_callback)(gint secs),
  void (*progress_callback)(gint secs),
  void (*tag_callback)(const gchar* tag, const gchar* value),
  void (*eos_callback)(), void (*error_callback)(const gchar* message));

void gssmp_gstreamer_pause ();
void gssmp_gstreamer_unpause ();

void gssmp_gstreamer_seek (gint secs);

void gssmp_gstreamer_set_volume (gdouble value);


