/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include <gnome.h>

void gssmp_files_init ();
void gssmp_files_quit ();

void gssmp_files_test_parent_dir ();

gchar* gssmp_files_find_next_file_after (const gchar* file_path);
gchar* gssmp_files_find_previous_file_before (const gchar* file_path);

gchar* gssmp_files_find_next_file_in_dir (const gchar* dir,
  const gchar* cur_file_name);

gchar* gssmp_files_find_previous_file_in_dir (const gchar* dir,
  const gchar* cur_file_name);

gchar* gssmp_files_get_parent_of_dir (const gchar* dir);

/**
 * Clear the cached shuffled list, e.g. because the user chose a different
 * directory.
 */
void gssmp_files_reset_shuffle ();

/**
 * Returns the next file in the cached shuffled list.  The returned string
 * must be freed by the caller.
 */
gchar* gssmp_files_find_next_shuffled_file ();

/**
 * Returns the previous file in the cached shuffled list.  The returned string
 * must be freed by the caller.
 */
gchar* gssmp_files_find_previous_shuffled_file ();

/**
 * Returns the absolute path of the supplied path, which may be relative.
 * The returned string must be freed by the caller.
 */
gchar* gssmp_files_make_absolute (const gchar* path);

