#!/bin/bash

G_SLICE=always-malloc G_DEBUG=gc-friendly valgrind --alignment=8 --num-callers=40 --leak-check=full --show-reachable=yes ./gssmp

