/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include "gssmp_files.h"

#include "gssmp_config.h"
#include "gssmp_util.h"

gchar* find_X_file_Y (const gchar* file_path, gboolean reverse);
gchar* gssmp_files_find_X_file_in_dir (const gchar* dir,
  const gchar* cur_file_name, gboolean reverse);
gchar* list_file_after (const gchar* dir, const gchar* file_name,
  gboolean reverse);
gchar* list_dir_after (const gchar* base_dir, const gchar* dir_name,
  gboolean reverse);
gchar* list_X_after (const gchar* base_dir, const gchar* file_name,
  GFileTest test_object_type, gboolean reverse);

gint reverse_g_utf8_collate (const gchar *str1, const gchar *str2);

gchar* get_selected_item_data ();
void build_shuffled_list ();
void add_files_to_shuffled_list (gchar* dirname);
void shuffle_list ();

GList *shuffled_list;
GList *selected_shuffled_item;

void gssmp_files_init ()
{
  shuffled_list          = NULL;
  selected_shuffled_item = NULL;
}

void gssmp_files_quit ()
{
  if (shuffled_list)
  {
    gssmp_util_free_list_and_data (shuffled_list);
  }
}

/**
 * Given the full path to a file, returns either the next file in this
 * directory, or, if there aren't any more, follows the usual rules (see
 * documentation for gssmp_files_find_next_file_in_dir) to find another file
 * inside the chose dir.
 *
 * The returned string must be freed by the caller.
 */
gchar* gssmp_files_find_next_file_after (const gchar* file_path)
{
  return find_X_file_Y (file_path, FALSE);
}

gchar* gssmp_files_find_previous_file_before (const gchar* file_path)
{
  return find_X_file_Y (file_path, TRUE);
}

gchar* find_X_file_Y (const gchar* file_path, gboolean reverse)
{
  gchar* dir;
  gchar* fl;
  gchar* ret;
  
  ret = NULL;
  
  if (file_path)
  {
    dir = g_path_get_dirname (file_path);
    fl = g_path_get_basename (file_path);
    ret = gssmp_files_find_X_file_in_dir (dir, fl, reverse);
    
    g_free (dir);
    g_free (fl);
  }
  
  return ret;
}

/**
 * Given the path of a directory, and the name (name only, not path) of
 * a file, looks for the next file in the directory supplied, and returns it
 * if found.  If it's not found, looks in this directory's siblings, and
 * continues up the directory tree (staying inside the main user-chosen
 * directory) until a file to play is found.
 *
 * If cur_file_name is the empty string, looks for the first file in
 * the supplied dir.
 *
 * If cur_file_name is NULL, looks inside any child directories we have (and
 * their children etc.) before looking for files inside the supplied dir.
 *
 * If no files are found inside the user-chosen dir, returns NULL.
 *
 * The returned string must be freed by the caller.
 */
gchar* gssmp_files_find_next_file_in_dir (const gchar* dir,
  const gchar* cur_file_name)
{
  return gssmp_files_find_X_file_in_dir (dir, cur_file_name, FALSE);
}

gchar* gssmp_files_find_previous_file_in_dir (const gchar* dir,
  const gchar* cur_file_name)
{
  return gssmp_files_find_X_file_in_dir (dir, cur_file_name, TRUE);
}

gchar* gssmp_files_find_X_file_in_dir (const gchar* dir,
  const gchar* cur_file_name, gboolean reverse)
{
  if (cur_file_name)
  {
    gchar* fl;
    fl = list_file_after (dir, cur_file_name, reverse);
    if (fl)
    {
      return fl;  // Found one in this dir - return it
    }
  }
  else
  {
    gchar* dr;
    dr = list_dir_after (dir, "", reverse);
    if (dr)
    {
      // Recurse into our first child and return the result
      gchar* child_result;
      child_result = gssmp_files_find_X_file_in_dir (dr, NULL, reverse);
      g_free (dr);
      return child_result;
    }
    else
    {
      // If no child dirs, look at the files in here
      gchar* fl;
      
      fl = list_file_after (dir, "", reverse);
      if (fl)
      {
        return fl;  // Found one in this dir - return it
      }
    }
  }

  // We have finished everything in this dir and its children
  // - move on to its sibling.
  if (strcmp (dir, current_directory) != 0 )
  {
    // Since the dirs are not equal, we must be inside current_directory
    gchar* parent;
    gchar* next_dir;
    gchar* ret;
    gchar* this_dir_name;
    
    // Look for our sibling
    this_dir_name = g_path_get_basename (dir);
    parent = gssmp_files_get_parent_of_dir (dir);
    next_dir = list_dir_after (parent, this_dir_name, reverse);
    
    if (next_dir)
    {
      // We have a sibling: recurse into it
      ret = gssmp_files_find_X_file_in_dir (next_dir, NULL, reverse);
      g_free (next_dir);
    }
    else
    {
      // Otherwise we look in our parent itself, skipping all the dirs
      // and just looking for files.
      ret = gssmp_files_find_X_file_in_dir (parent, "", reverse);
    }
    
    g_free (parent);
    g_free (this_dir_name);
    
    return ret;
  }
  
  // Otherwise, we have looked everwhere we can, and we found nothing.
  return NULL; 
}

/**
 * Finds the parent directory of dir (which is itself a directory).
 *
 * The returned string must be freed by the caller.
 */
gchar* gssmp_files_get_parent_of_dir (const gchar* dir)
{
	gchar *top_dir;
  gchar *parent_dir;
  int inc;
  int len;
  
  parent_dir = NULL;
  len = strlen (dir);
  
  //if (len > 0)
  //{
    inc = 1;
    if (dir[-1] == G_DIR_SEPARATOR)
    {
      ++inc;
    }
    
    top_dir = g_path_get_basename (dir);
    parent_dir = g_strndup (dir, strlen (dir) - (strlen (top_dir) + inc));
    
    g_free (top_dir);
  //}
  return parent_dir;
}

void gssmp_files_reset_shuffle ()
{
  if (shuffled_list)
  {
    gssmp_util_free_list_and_data (shuffled_list);
    shuffled_list = NULL;
    selected_shuffled_item = NULL;
  }
}

gchar* gssmp_files_find_next_shuffled_file ()
{
  // If we don't have a shuffled list, create one and point at the
  // first element
  if (!shuffled_list)
  {
    build_shuffled_list ();
    selected_shuffled_item = shuffled_list;
  }
  else
  {
    // If we do have one, just move forwards through the list
    selected_shuffled_item = g_list_next (selected_shuffled_item);
  }
  
  return get_selected_item_data ();
}

gchar* gssmp_files_find_previous_shuffled_file ()
{
  // If we don't have a shuffled list, create one and point at the
  // first element
  if (!shuffled_list)
  {
    build_shuffled_list ();
    selected_shuffled_item = shuffled_list;
  }
  else
  {
    // If we do have one, just move backwards through the list
    selected_shuffled_item = g_list_previous (selected_shuffled_item);
  }
  
  return get_selected_item_data ();
}

gchar* get_selected_item_data ()
{
  if (selected_shuffled_item)
  {
    return g_strdup (selected_shuffled_item->data);
  }
  
  // There is no selected item: build a list and return the first item.
  if (shuffled_list)
  {
    gssmp_util_free_list_and_data (shuffled_list);
    shuffled_list = NULL;
  }
  build_shuffled_list ();
  selected_shuffled_item = shuffled_list;
  
  if (selected_shuffled_item)
  {
    return g_strdup (selected_shuffled_item->data);
  }
  else
  {
    return NULL;
  }
}

gchar* gssmp_files_make_absolute (const gchar* path)
{
  gchar* ret;
  
  if (g_path_is_absolute (path))
  {
    ret = g_strdup (path);
  }
  else
  {
    gchar* cur_dir;
    cur_dir = g_get_current_dir ();
    ret = g_build_filename (cur_dir, path, NULL);
    g_free (cur_dir);
  }
  
  return ret;
}

// -------------------------------------

/**
 * Finds the file alphabetically after file_name inside dir, and returns its
 * full path.
 * 
 * If file_name is the last file in dir, returns NULL.
 *
 * The returned string must be freed by the caller.
 */
gchar* list_file_after (const gchar* base_dir, const gchar* file_name,
  gboolean reverse)
{
  return list_X_after (base_dir, file_name, G_FILE_TEST_IS_REGULAR, reverse);
}

gint reverse_g_utf8_collate (const gchar *str1, const gchar *str2)
{
  return -g_utf8_collate (str1, str2);
}

/**
 * Finds the directory alphabetically after dir_name inside base_dir.
 * 
 * If dir_name is the last directory in base_dir, returns NULL.
 *
 * The returned string must be freed by the caller.
 */
gchar* list_dir_after (const gchar* base_dir, const gchar* dir_name,
  gboolean reverse)
{
  return list_X_after (base_dir, dir_name, G_FILE_TEST_IS_DIR, reverse);
}

gchar* list_X_after (const gchar* base_dir, const gchar* file_name,
  GFileTest test_object_type, gboolean reverse)
{
  GSList *dir_list;
  GSList *found_list_element;
  GDir *dir;
  const gchar *name;
  gchar *ret;
  
  // Return NULL by default
  ret = NULL;
  
  // Do nothing if the user selected a non-existent directory
  if (g_file_test (base_dir, G_FILE_TEST_IS_DIR))
  {
    GCompareFunc compare_func;
    
    if (reverse)
    {
      compare_func = (GCompareFunc)reverse_g_utf8_collate;
    }
    else
    {
      compare_func = (GCompareFunc)g_utf8_collate;
    }
    
    // Create an empty list
    dir_list = NULL;
    
    // Add all the files in the dir into the list,
    // sorted alphabetically
    dir = g_dir_open (base_dir, 0, NULL);
    while ((name = g_dir_read_name (dir)))
    {
      gchar* full_path;
      full_path = g_build_filename (base_dir, name, NULL);
      
      if (g_file_test (full_path, test_object_type))
      {
        dir_list = g_slist_insert_sorted (dir_list, (gpointer)name, 
          compare_func);
      }
      g_free (full_path);
    }
    
    if (strlen (file_name) > 0)
    {
      // Find the file by name
      found_list_element = g_slist_find_custom (dir_list, file_name, 
        compare_func);
      
      if (found_list_element)
      {
        found_list_element = g_slist_next (found_list_element);
      }
    }
    else
    {
      // Start at the beginning of the list
      found_list_element = dir_list;
    }
    
    if (found_list_element)
    {
      ret = g_build_filename (base_dir, found_list_element->data, NULL );
    }
    
    g_free (dir);
    g_slist_free (dir_list);
  }
  
  return ret;
}

/**
 * Search through the current directory and put every valid file into a
 * queue, then shuffle the queue so it is random.
 */
void build_shuffled_list ()
{
  add_files_to_shuffled_list (current_directory);
  
  shuffle_list ();
}

void add_files_to_shuffled_list (gchar* dirname)
{
  GDir *dir;
  const gchar *name;
  
  dir = g_dir_open (dirname, 0, NULL);
  while ((name = g_dir_read_name (dir)))
  {
    gchar* full_path;
    full_path = g_build_filename (dirname, name, NULL);
    
    if (g_file_test (full_path, G_FILE_TEST_IS_REGULAR))
    { // If it's a file, add it to our (currently un-)shuffled list
      shuffled_list = g_list_append (shuffled_list, full_path);
    }
    else if (g_file_test (full_path, G_FILE_TEST_IS_DIR))
    { // If it's a dir, recurse into it
      add_files_to_shuffled_list (full_path);
      g_free (full_path);
    }
    else
    {
      g_free (full_path);
    }
  }
}

/**
 * Perform a Knuth Shuffle on the list of tracks.
 */
void shuffle_list ()
{
  if (shuffled_list)
  {
    GRand *rand;
    guint list_length;
    guint current_item_number;
    GList *current_item;
    
    rand = g_rand_new ();
    
    list_length = g_list_length (shuffled_list);
    
    for (current_item_number = 0, current_item = shuffled_list;
         current_item_number < list_length - 1 && current_item;
         ++current_item_number, current_item = g_list_next (current_item))
    {
      gint32 offset;
      gchar* tmp;
      GList *item_to_swap_with;
      
      offset = g_rand_int_range (rand, 0, list_length - current_item_number);
      item_to_swap_with = g_list_nth (current_item, offset);
      
      tmp = current_item->data;
      current_item->data = item_to_swap_with->data;
      item_to_swap_with->data = tmp;
    }
    
    g_rand_free (rand);
  }
}








