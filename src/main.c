/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "gssmp_config.h"
#include "gssmp_ui.h"
#include "gssmp_gstreamer.h"
#include "gssmp_files.h"
#include "gssmp_test.h"

int
main (int argc, char *argv[])
{
  
  /*{ // DEBUG
    gssmp_test_shuffle (argc, argv);
    return 0;
  }*/
  
#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE,
                      argc, argv,
                      GNOME_PARAM_APP_DATADIR, PACKAGE_DATA_DIR,
                      NULL);
  
  gssmp_files_init ();
  gssmp_gstreamer_init (argc, argv);
  gssmp_config_init ();
  gssmp_ui_init (argc, argv);
  
  gtk_main ();
  
  gssmp_files_quit ();
  gssmp_gstreamer_quit ();
  gssmp_config_quit ();
  
  return 0;
}


