/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include <gnome.h>

enum repeatstyle
{
  no_repeat,
  repeat_all,
  repeat_one,
  shuffle
};

extern gchar* current_directory;  // This should not be set directly -
                                  // use gssmp_config_set_directory

extern gchar* current_filename;
extern gint   current_time;
extern gint   current_length;

extern gint             config_num_bookmarks; // The max length of the list
extern gboolean         config_paused;        // Are we paused?
extern enum repeatstyle config_repeat_style;  // Shuffle, repeat etc.

extern gboolean config_lastfm_active;   // Do we report to last.fm?
extern gchar*   config_lastfm_exe;      // The executable to use to report

typedef struct
{
  gchar* track;
  gchar* artist;
  gchar* album;
  gchar* directory;
  gchar* filename;
  gint time;
  gint length;
} GssmpBookmark;

void gssmp_config_init ();
void gssmp_config_quit ();

/**
 * Copy the supplied string into the current_directory variable,
 * without the ending '/', if present.
 */
void gssmp_config_set_directory (const gchar* cfg_dir);

/**
 * Adds a bookmark to the top of the list of bookmarks.  Makes copies
 * of the strings passed in.
 */
void gssmp_config_add_bookmark (const gchar* track, const gchar* artist,
  const gchar* album, const gchar* directory, const gchar* filename,
  gint time, gint length);

GssmpBookmark* gssmp_config_get_bookmark (guint number);

void gssmp_config_bookmarks_foreach (
	void (*callback) (GssmpBookmark*, gpointer user_data),
	gpointer user_data);

guint gssmp_config_bookmarks_length ();

