/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include <gnome.h>

#include "gssmp_files.h"
#include "gssmp_ui.h"
#include "gssmp_config.h"
#include "gssmp_gstreamer.h"

void gssmp_test_shuffle (int argc, char *argv[])
{
  gint i;
  
  gssmp_files_init ();
  gssmp_gstreamer_init (argc, argv);
  gssmp_config_init ();
  
  current_filename     = "/home/andy/iRiver/Albums/Nine_Inch_Nails/Broken/"
                         "01 - Pinion.ogg";
  current_directory    = "/home/andy/iRiver/Albums/Nine_Inch_Nails/Broken";
  
  config_repeat_style = shuffle;
  
  for (i=0; i<20; ++i)
  {
    gchar* filename;
    
    filename = gssmp_files_find_next_shuffled_file ();
    
    g_free (filename);
  }
  
  gssmp_files_quit ();
  gssmp_gstreamer_quit ();
  gssmp_config_quit ();
}

void gssmp_test_traverse_dirs ()
{
  {
    gchar* first_file_name;
    //current_directory = "/home/andy/iRiver/Albums/A_Perfect_Circle/";
    current_directory = "/home/andy/iRiver/Albums/Madonna";
    current_filename = NULL;
    
    first_file_name = gssmp_files_find_next_file_in_dir (current_directory,
      NULL);
  
    if (first_file_name)
    {
      gssmp_ui_play_file (first_file_name, FALSE, TRUE, 0, TRUE);
    }
    
    while (current_filename)
    {
      sleep (1);
      gssmp_ui_next_clicked ();
    }
    
  }
}

void gssmp_files_test_parent_dir ()
{
  gchar* parent;
  
  parent = gssmp_files_get_parent_of_dir ("/home/andy/iRiver");
  printf ( "gssmp_test_parent_dir: '/home/andy/iRiver' -> '%s'\n",  parent);
  g_free (parent);
  
  parent = gssmp_files_get_parent_of_dir ("/home/andy/iRiver/");
  printf ( "gssmp_test_parent_dir: '/home/andy/iRiver/' -> '%s'\n", parent );
  g_free (parent);
  
}

void dur_cb (gint secs)
{
  printf ("dur: %d\n", secs);
}

void prg_cb (gint secs)
{
  printf ("prg: %d\n", secs);
}

void tag_cb (const gchar* tag, const gchar* value)
{
  printf ("tag: %s -> %s\n", tag, value);
}

void eos_cb ()
{
  printf ("eos\n");
}

void error_cb (const gchar* msg)
{
  printf ("error: %s\n", msg);
}

void gssmp_test_gstreamer (int argc, char *argv[])
{
  gssmp_gstreamer_init (argc, argv);
  
  gssmp_gstreamer_play (
    "/home/andy/iRiver/Albums/Amon_Tobin/Bricolage/01 - Stoney_Street.ogg", 0,
    dur_cb, prg_cb, tag_cb, eos_cb, error_cb );
  
  gtk_main ();
}

