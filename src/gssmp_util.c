#include "gssmp_util.h"

void gssmp_util_free_queue_and_data (GQueue *queue)
{
  while (!g_queue_is_empty (queue))
  {
    gpointer msg;
    msg = g_queue_pop_tail (queue);
    g_free (msg);
  }
  g_queue_free (queue);
}

void gssmp_util_free_list_and_data (GList *list)
{
  GList *item;
  
  item = list;
  while ((item = g_list_next(item)))
  {
    g_free (item->data);
  }
  g_list_free (list);
}

