/* 
 *  Gnome Simple Stateful Music Player
 *  Copyright (C) 2007 Andy Balaam
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**/

#include "gssmp_ui.h"

#include <stdio.h>
#include <libgnomevfs/gnome-vfs.h>
#include <sys/wait.h>

#include "interface.h"
#include "support.h"
#include "gssmp_config.h"
#include "gssmp_files.h"
#include "gssmp_gstreamer.h"
#include "gssmp_util.h"

#define STATUS_TIMEOUT_LENGTH 5000

// Note: DOC_URI is passed in from the Makefile
#define BIN_PKG_DOC_PATH "../share/doc/gssmp/html/use.html"
#define SRC_DIR_DOC_PATH "../www/use.html"
#define ONLINE_DOCS_URI  "http://gssmp.sourceforge.net/use.html"

GtkWindow *window1;
GtkWindow *aboutdialog1;
GtkWidget *hscale1;
GtkLabel *label4;     // Artist and track name
GtkLabel *label5;     // Album name etc.
GtkLabel *label7;     // Current time
GtkLabel *label8;     // Track length
GtkWidget *button2;   // Play
GtkWidget *button3;   // Pause
GtkStatusbar *statusbar1;
GtkMenuItem *recent1;
GtkCheckMenuItem *no_repeat1;
GtkCheckMenuItem *repeat_all1;
GtkCheckMenuItem *repeat_one1;
GtkCheckMenuItem *shuffle1;

// last.fm stuff
GtkWindow *lastfmwindow;
GtkCheckButton* checkbutton1;
GtkEntry* entry1;

gchar* stored_title;
gchar* stored_track_number;
gchar* stored_track_length;
gchar* stored_track_count;
gchar* stored_album;
gchar* stored_artist;

guint context_id_play_file;
guint context_id_error_playing_file;
guint context_id_status_callback;
guint context_id_not_a_file;

gulong menu_number;

gboolean skip_errors;     // Don't pop up an error when reading an invalid file
gboolean moving_forward;  // TRUE unless we have just clicked "Previous"
guint status_timeout_source;

int new_tag;  // 0 = first time, 1 = info received, 2 = no info

void gssmp_ui_choose_bookmark_clicked (GtkMenuItem* unused, gpointer num);

void display_track_info (gboolean from_bookmark);
void fork_lastfm_exe ();
void choose_repeat_menu_item ();

void add_bookmark_here ();
void redisplay_bookmarks ();
void menu_bookmark_callback_create (GssmpBookmark* bmk, gpointer submenu);

void play_bookmark (guint num);

struct StatusDetails
{
  guint context_id;
  const gchar* msg;
  const gchar* sub;
};

gboolean status_callback (gpointer details);

void play_in_dir (const gchar* dirname);

void lastfm_enable_buttons ();

/**
 * Returns a newly-allocated string saying the time e.g. "1m 30s"
 */
gchar* secs_to_time_str (gint secs);

void gssmp_ui_init (int argc, char *argv[])
{
  window1 = GTK_WINDOW (create_window1 ());
  aboutdialog1 = NULL;
  hscale1 = lookup_widget (GTK_WIDGET (window1), "hscale1");
  label4 = GTK_LABEL (lookup_widget (GTK_WIDGET (window1), "label4"));
  label5 = GTK_LABEL (lookup_widget (GTK_WIDGET (window1), "label5"));
  label7 = GTK_LABEL (lookup_widget (GTK_WIDGET (window1), "label7"));
  label8 = GTK_LABEL (lookup_widget (GTK_WIDGET (window1), "label8"));
  button2 = lookup_widget (GTK_WIDGET (window1), "button2");
  button3 = lookup_widget (GTK_WIDGET (window1), "button3");
  statusbar1 = GTK_STATUSBAR (lookup_widget (GTK_WIDGET (window1),
    "statusbar1"));
  recent1     = GTK_MENU_ITEM (lookup_widget (GTK_WIDGET (window1), "recent1"));
  no_repeat1  = GTK_CHECK_MENU_ITEM (lookup_widget (GTK_WIDGET (window1),
    "no_repeat1")); 
  repeat_all1 = GTK_CHECK_MENU_ITEM (lookup_widget (GTK_WIDGET (window1), 
    "repeat_all1"));
  repeat_one1 = GTK_CHECK_MENU_ITEM (lookup_widget (GTK_WIDGET (window1),
    "repeat_one1"));
  shuffle1    = GTK_CHECK_MENU_ITEM (lookup_widget (GTK_WIDGET (window1), 
    "shuffle1"));
  
  lastfmwindow = NULL;
  checkbutton1 = NULL;
  entry1 = NULL;
  
  // NOTE: ICON_FILE is passed in via the make file
  gtk_window_set_icon_from_file (window1, ICON_FILE, NULL);
  
  choose_repeat_menu_item ();
    
  context_id_play_file = gtk_statusbar_get_context_id (statusbar1,
    "gssmp_ui_play_file");
  context_id_error_playing_file = gtk_statusbar_get_context_id (statusbar1,
    "gssmp_ui_error_callback");
  context_id_status_callback = gtk_statusbar_get_context_id (statusbar1,
    "status_callback");
  context_id_not_a_file = gtk_statusbar_get_context_id (statusbar1,
    "gssmp_ui_init");
  
  skip_errors = TRUE;
  moving_forward = TRUE;
  
  status_timeout_source = 0;
  
  
  
  gtk_widget_show (GTK_WIDGET (window1));

  if (argc < 2)
  {
    play_bookmark (0);
  }
  else
  {
    gchar* file_or_dir = gssmp_files_make_absolute (argv[1]);
    
    if (g_file_test (file_or_dir, G_FILE_TEST_IS_DIR))
    {
      play_in_dir (file_or_dir);
      g_free (file_or_dir);
    }
    else if (g_file_test (file_or_dir, G_FILE_TEST_IS_REGULAR))
    {
      gssmp_ui_play_file (file_or_dir, TRUE, FALSE, 0, FALSE);
    }
    else
    {
      gssmp_ui_display_status_message ( context_id_not_a_file,
        _("Could not find file '%s'"), file_or_dir );
      g_free (file_or_dir);
    }
  }
  
  redisplay_bookmarks ();
}

void gssmp_ui_quit ()
{
  add_bookmark_here ();
  
  g_free (stored_title);
  g_free (stored_track_number);
  g_free (stored_track_count);
  g_free (stored_album);
  g_free (stored_artist);
  
  gtk_main_quit ();
}

void gssmp_ui_about ()
{
  if (!aboutdialog1)
  {
    aboutdialog1 = GTK_WINDOW (create_aboutdialog1 ());
  }
  
  gtk_widget_show (GTK_WIDGET (aboutdialog1));
}

void update_buttons ()
{
  gtk_widget_set_sensitive (button2, config_paused);
  gtk_widget_set_sensitive (button3, !config_paused);
}

void gssmp_ui_choose_directory_clicked ()
{
  GtkWidget *dialog;
  
  dialog = gtk_file_chooser_dialog_new (_("Choose Directory"),
    window1, GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
    GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
    NULL);
  
  if (current_directory && strlen (current_directory) > 0)
  {
    gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog),
      current_directory);
  }
  else
  {
    gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog),
      g_get_home_dir());
  }

  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    gchar *dirname;
    
    gssmp_files_reset_shuffle ();
    
    dirname = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
    
    play_in_dir (dirname);
    
    g_free (dirname);
  }
  gtk_widget_destroy (dialog);
}

void gssmp_ui_choose_file_clicked ()
{
  GtkWidget *dialog;

  dialog = gtk_file_chooser_dialog_new (_("Choose File"),
    window1, GTK_FILE_CHOOSER_ACTION_OPEN,
    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
    GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
    NULL);
  
  if (current_filename && strlen (current_filename) > 0)
  {
    gchar* dir;
    dir = g_path_get_dirname (current_filename);
    gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), dir);
    g_free (dir);
  }
  else if (current_directory && strlen (current_directory) > 0)
  {
    gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog),
      current_directory);
  }
  else
  {
    gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog),
      g_get_home_dir());
  }
  
  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    gchar *filename;
    
    gssmp_files_reset_shuffle ();
    
    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
    if (filename)
    {
      add_bookmark_here (); // Remember what we were playing
      gssmp_ui_play_file (filename, TRUE, TRUE, 0, FALSE);
    }
  }
  gtk_widget_destroy (dialog);
}

void gssmp_ui_eos_callback ()
{
  if (config_repeat_style == repeat_one)
  {
    gssmp_gstreamer_seek (0);
  }
  else
  {
    gssmp_ui_next_clicked ();
  }
}

void gssmp_ui_next_clicked ()
{
  gchar *filename;
  
  moving_forward = TRUE;
  
  if (config_repeat_style == shuffle)
  {
    filename = gssmp_files_find_next_shuffled_file ();
  }
  else
  {
    filename = gssmp_files_find_next_file_after (current_filename);
    
    if (!filename && (config_repeat_style == repeat_all))
    {
      filename = gssmp_files_find_next_file_in_dir (current_directory, NULL);
    }
  }
  
  if (filename)
  {
    gssmp_ui_play_file (filename, FALSE, TRUE, 0, TRUE);
  }
  else
  {
    gssmp_ui_pause_clicked ();
  }
}

void gssmp_ui_previous_clicked ()
{
  gchar *filename;
  
  moving_forward = FALSE;
  
  filename = gssmp_files_find_previous_file_before (current_filename);
  
  if (filename)
  {
    gssmp_ui_play_file (filename, FALSE, TRUE, 0, TRUE);
  }
  else
  {
    gssmp_ui_pause_clicked ();
  }
}

void gssmp_ui_display_status_message (guint context_id, const gchar* msg,
  const gchar* sub)
{
  gchar* full_msg;
  
  full_msg = g_strdup_printf (msg, sub);
  
  gtk_statusbar_pop (statusbar1, context_id_status_callback);
  gtk_statusbar_push (statusbar1, context_id_status_callback, full_msg);

  printf ("%s\n", full_msg);

  g_free (full_msg);
}

void gssmp_ui_display_2_status_messages (guint context_id, const gchar* msg1,
  const gchar* sub1, const gchar* msg2, const gchar* sub2)
{
  struct StatusDetails* details;
  
  gssmp_ui_display_status_message (context_id, msg1, sub1);
  
  details = g_slice_new (struct StatusDetails);
  details->context_id = context_id;
  details->msg = msg2;
  details->sub = sub2;
  
  if (status_timeout_source)
  {
    g_source_remove (status_timeout_source);
  }
  status_timeout_source = g_timeout_add (STATUS_TIMEOUT_LENGTH,
    status_callback, (gpointer)details);
}

/**
 * Play the sound file supplied by calling into the gstreamer code and making
 * the UI ready where required.
 *
 * @arg filename             The full path to a file on disk
 * @arg change_dir           If this file is outside the user's chosen
 *                           directory, should we change that directory to
 *                           match the file?
 * @arg new_filename         Is the filename a newly-allocated string?  I.e.
 *                           should we free the old current_filename?
 * @arg start_offset         How many seconds through the file should we start?
 * @arg skip_to_next_if_fail If this file fails to load, automatically skip to
 *                           the next?  (If not, we display an error to the
 *                           user.)
 */
void gssmp_ui_play_file (gchar* filename, gboolean change_dir,
  gboolean new_filename, gint start_offset, gboolean skip_to_next_if_fail)
{
  skip_errors = skip_to_next_if_fail;
  
  if (change_dir)
  {
    if (!current_directory ||
      strncmp (current_directory, filename, strlen (current_directory)) != 0)
    {
      current_directory = g_path_get_dirname (filename);
    }
  }
  
  if (new_filename && current_filename)
  {
    g_free (current_filename);
  }
  current_filename = filename;
  
  gssmp_ui_clear_track_info ();
  
  new_tag = 0;
  
  config_paused = FALSE;
  update_buttons ();
  
  gssmp_ui_display_2_status_messages( context_id_play_file,
    _("Playing file %s"), current_filename,
    _("Playing in directory %s"), current_directory );
  
  gssmp_gstreamer_play (current_filename, start_offset,
    gssmp_ui_duration_callback, gssmp_ui_progress_callback,
    gssmp_ui_tag_callback, gssmp_ui_eos_callback,
    gssmp_ui_error_callback);
}

void gssmp_ui_play_clicked ()
{
  config_paused = FALSE;
  update_buttons ();
  gssmp_gstreamer_unpause ();
}

void gssmp_ui_pause_clicked ()
{
  config_paused = TRUE;
  update_buttons ();
  gssmp_gstreamer_pause ();
}

void gssmp_ui_slider_changed (gdouble value)
{
  gchar* time_str;
  gint secs;
  
  secs = (gint)value;
  if (current_length != -1 && secs >= current_length)
  {
    secs = current_length - 1;
  }
  
  gssmp_gstreamer_seek (secs);
  
  current_time = secs;
  
  time_str = secs_to_time_str (secs);
  gtk_label_set_text (label7, time_str);
  gtk_widget_queue_draw(GTK_WIDGET(label7));
  g_free (time_str);
}

void gssmp_ui_volume_change (gdouble value)
{
  if (value < 0)
  {
    value = 0;
  }
  else if (value > 1)
  {
    value = 1;
  }
  gssmp_gstreamer_set_volume (value);
}

void gssmp_ui_progress_callback (gint secs)
{
  gchar* time_str;
  
  current_time = secs;
  gtk_range_set_value (GTK_RANGE(hscale1), (gdouble)secs);
  
  time_str = secs_to_time_str (secs);
  gtk_label_set_text (label7, time_str);
  
  switch (new_tag)
  {
    case 1:
    {
      display_track_info (FALSE);
      new_tag = 2;
      break;
    }
    case 0:
    {
      new_tag = 1;
      break;
    }
    default:
    {
      break;
    }
  }
  
  gtk_widget_queue_draw(GTK_WIDGET(hscale1));
  gtk_widget_queue_draw(GTK_WIDGET(label7));
  g_free (time_str);
}

void gssmp_ui_duration_callback (gint secs)
{
  gtk_range_set_range (GTK_RANGE(hscale1), (gdouble)0, (gdouble)secs);
  
  stored_track_length = secs_to_time_str (secs);
  current_length = secs;
}

void gssmp_ui_tag_callback (const gchar* tag, const gchar* value)
{
  new_tag = 1;
  if (strcmp(tag, "title") == 0)
  {
    stored_title = g_strdup (value);
  }
  else if (strcmp(tag, "artist") == 0)
  {
    stored_artist = g_strdup (value);
  }
  else if (strcmp(tag, "album") == 0)
  {
    stored_album = g_strdup (value);
  }
  else if (strcmp(tag, "track-number") == 0)
  {
    stored_track_number = g_strdup (value);
  }
  else if (strcmp(tag, "track-count") == 0)
  {
    stored_track_count = g_strdup (value);
  }
  else
  {
    fprintf (stderr, _("Unknown tag: %s -> %s\n"), tag, value);
  }
}

void gssmp_ui_error_callback (const gchar* msg)
{
  gssmp_ui_display_status_message (context_id_error_playing_file,
    _("Unable to play file: %s"), current_filename );
  
  if (skip_errors)
  {
    if (moving_forward)
    {
      gssmp_ui_next_clicked ();
    }
    else
    {
      gssmp_ui_previous_clicked ();
      moving_forward = FALSE;
    }
  }
  else
  {
    GtkMessageDialog* dialog;
    dialog = GTK_MESSAGE_DIALOG (gtk_message_dialog_new (window1,
                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                    GTK_MESSAGE_ERROR,
                                    GTK_BUTTONS_CLOSE,
                                    _("Error playing file '%s': %s"),
                                    current_filename, msg));
   gtk_dialog_run (GTK_DIALOG (dialog));
   gtk_widget_destroy (GTK_WIDGET (dialog));
  }
}

void gssmp_ui_clear_track_info ()
{
  if (stored_title)
  {
    g_free (stored_title);
  }
  if (stored_track_number)
  {
    g_free (stored_track_number);
  }
  if (stored_track_length)
  {
    g_free (stored_track_length);
  }
  if (stored_track_count)
  {
    g_free (stored_track_count);
  }
  if (stored_artist)
  {
    g_free (stored_artist);
  }
  if (stored_album)
  {
    g_free (stored_album);
  }
  
  if (current_filename)
  {
    gchar* tmp1;
    gchar* tmp2;
    
    stored_title = g_path_get_basename (current_filename);
    tmp1 = g_path_get_dirname (current_filename);
    stored_album = g_path_get_basename (tmp1);
    tmp2 = gssmp_files_get_parent_of_dir (tmp1);
    stored_artist = g_path_get_basename (tmp2);
    
    g_free (tmp1);
    g_free (tmp2);
  }
  else
  {
    stored_title = NULL;
    stored_album = NULL;
    stored_artist = NULL;
  }
  
  stored_track_number = NULL;
  stored_track_length = NULL;
  stored_track_count  = NULL;
}

// --------------------------------

#define MAX_TITLE_LENGTH 2048
void display_track_info (gboolean from_bookmark)
{
  gchar title[MAX_TITLE_LENGTH];
  gchar tmp[MAX_TITLE_LENGTH];
  
  g_strlcpy (title, "<span size=\"large\">", MAX_TITLE_LENGTH);
  
  if (stored_artist)
  {
    if (stored_title)
    {
      if (stored_track_number)
      {
        if (stored_track_count)
        {
          g_snprintf (tmp, MAX_TITLE_LENGTH, _("%s - %s (%s/%s)"),
            stored_artist, stored_title, stored_track_number,
            stored_track_count);
        }
        else
        {
          g_snprintf (tmp, MAX_TITLE_LENGTH, _("%s - %s (%s)"),
            stored_artist, stored_title, stored_track_number);
        }
      }
      else
      {
        g_snprintf (tmp, MAX_TITLE_LENGTH, _("%s - %s"),
          stored_artist, stored_title);
      }
    }
    else
    {
      g_snprintf (tmp, MAX_TITLE_LENGTH, _("%s - unnamed"), stored_artist);
    }
  }
  else
  {
    if (stored_title)
    {
      if (stored_track_number)
      {
        if (stored_track_count)
        {
          g_snprintf (tmp, MAX_TITLE_LENGTH, _("%s (%s/%s)"),
            stored_title, stored_track_number, stored_track_count);
        }
        else
        {
          g_snprintf (tmp, MAX_TITLE_LENGTH, _("%s (%s)"),
            stored_title, stored_track_number);
        }
      }
      else
      {
        g_snprintf (tmp, MAX_TITLE_LENGTH, _("%s"), stored_title);
      }
    }
    else
    {
      g_strlcpy (tmp, "", MAX_TITLE_LENGTH);
    }
  }
  
  g_strlcat (title, tmp, MAX_TITLE_LENGTH);
  
  g_strlcat (title, "</span>", MAX_TITLE_LENGTH);
  
  gtk_label_set_markup (label4, title);
  
  // Now the album name
  if (stored_album)
  {
    g_snprintf (tmp, MAX_TITLE_LENGTH, _("Album: %s"), stored_album);
  }
  else
  {
    g_strlcpy (tmp, "", MAX_TITLE_LENGTH);
  }
  
  gtk_label_set_text (label5, tmp);
  
  // and the track length
  if (stored_track_length)
  {
    g_snprintf (tmp, MAX_TITLE_LENGTH, _("Length: %s"), stored_track_length);
  }
  else
  {
    g_strlcpy (tmp, "", MAX_TITLE_LENGTH);
  }
  gtk_label_set_text (label8, tmp);
  
  if( config_lastfm_active &&
    current_length > 0 && stored_title && stored_artist
    && !from_bookmark )
  {
    fork_lastfm_exe ();
  }
  
  add_bookmark_here (); // Put this track to the top of the bookmarks list
}

void fork_lastfm_exe ()
{
  pid_t pid;
  char tmp[32];
  
  pid = fork();
  
  if (pid == -1)
  {
      fprintf (stderr, "fork() error before launching lastfm exe.\n");
  }
  else if( pid == 0 )
  {
    // We are the child, run lastfm exe
    
    char *args[10];
    
    args[0] = config_lastfm_exe;
    args[1] = "--length";
    g_snprintf (tmp, 32, "%d", current_length);
    args[2] = tmp;
    args[3] = "--title";
    args[4] = stored_title;
    args[5] = "--artist";
    args[6] = stored_artist;
    
    if( stored_album )
    {
      args[7] = "--album";
      args[8] = stored_album;
      args[9] = NULL;

      fprintf (stderr, "Running: %s %s %s %s \"%s\" %s \"%s\" %s \"%s\"\n",
        args[0], args[1], args[2], args[3], args[4],
        args[5], args[6], args[7], args[8]);
    }
    else
    {
      args[7] = NULL;

      fprintf (stderr, "Running: %s %s %s %s \"%s\" %s \"%s\"\n",
        args[0], args[1], args[2], args[3], args[4],
        args[5], args[6]);
    }
    
    execvp( args[0], args );
    
    // Should never get here.
    fprintf (stderr, "execvp() error launching lastfm exe '%s'.\n",
      config_lastfm_exe);
  }
  else
  {
	  int status;
	  waitpid( pid, &status, 0 );
	  // TODO report status anomalies
  }
}

gchar* secs_to_time_str (gint secs)
{
  if (secs < 0)
  {
    secs = 0;
  }
  
  guint mins = secs / 60;
  guint remaining_secs = secs % 60;
  
  return g_strdup_printf (_("%dm %02ds"), mins, remaining_secs);
}

void gssmp_ui_choose_bookmark_clicked (GtkMenuItem* unused, gpointer num)
{
  gssmp_files_reset_shuffle ();
  config_paused = FALSE;
  play_bookmark ((gulong)num);
}

void gssmp_ui_launch_help ()
{
  gchar* uri;
  
  if (g_file_test (DOC_URI, G_FILE_TEST_IS_REGULAR))
  {
    uri = gnome_vfs_get_uri_from_local_path (DOC_URI);
  }
  else if (g_file_test (BIN_PKG_DOC_PATH, G_FILE_TEST_IS_REGULAR))
  {
    gchar* abs;
    abs = gssmp_files_make_absolute (BIN_PKG_DOC_PATH);
    uri = gnome_vfs_get_uri_from_local_path (abs);
    g_free (abs);
  }
  else if (g_file_test (SRC_DIR_DOC_PATH, G_FILE_TEST_IS_REGULAR))
  {
    gchar* abs;
    abs = gssmp_files_make_absolute (SRC_DIR_DOC_PATH);
    uri = gnome_vfs_get_uri_from_local_path (abs);
    g_free (abs);
  }
  else
  {
    uri = g_strdup (ONLINE_DOCS_URI);
  }
  
  gnome_vfs_url_show (uri);
  
  g_free (uri);
}

void gssmp_ui_lastfm ()
{
  if (!lastfmwindow)
  {
    lastfmwindow = GTK_WINDOW (create_lastfmwindow ());
    checkbutton1 = GTK_CHECK_BUTTON (
      lookup_widget (GTK_WIDGET (lastfmwindow), "checkbutton1"));
    entry1 = GTK_ENTRY (lookup_widget (GTK_WIDGET (lastfmwindow), "entry1"));
  }
  
  gtk_toggle_button_set_active (
    GTK_TOGGLE_BUTTON (checkbutton1), config_lastfm_active);
  gtk_entry_set_text (entry1, config_lastfm_exe);
  
  lastfm_enable_buttons ();
  
  gtk_widget_show (GTK_WIDGET (lastfmwindow));
}

void gssmp_ui_lastfm_close ()
{
  gtk_widget_hide (GTK_WIDGET (lastfmwindow));
}

void gssmp_ui_lastfm_statechange ()
{
  config_lastfm_active = gtk_toggle_button_get_active (
    GTK_TOGGLE_BUTTON (checkbutton1));
}

void gssmp_ui_lastfm_exechanged ()
{
  lastfm_enable_buttons ();
  
  if (config_lastfm_exe)
  {
    g_free (config_lastfm_exe);
  }
  config_lastfm_exe = g_strdup( gtk_entry_get_text (entry1) );
}

// --------------------------------

void add_bookmark_here ()
{
  if (current_filename && stored_title)
  {
    gssmp_config_add_bookmark (stored_title, stored_artist, stored_album,
      current_directory, current_filename, current_time, current_length);
    redisplay_bookmarks ();
  }
}

void play_bookmark (guint num)
{
  gboolean keep_paused;
  GssmpBookmark *bmk;
  
  new_tag = 0;
  stored_track_number = NULL;
  stored_track_count  = NULL;
  stored_track_length = NULL;
  
  bmk = gssmp_config_get_bookmark (num);
  if (bmk)
  {
    add_bookmark_here ();   // First remember what we were playing before
    
    gssmp_ui_duration_callback (bmk->length);
    gssmp_ui_progress_callback (bmk->time);
    
    if (stored_title)
    {
      g_free (stored_title);
    }
    stored_title  = g_strdup (bmk->track);
    
    if (stored_album)
    {
      g_free (stored_album);
    }
    stored_album  = g_strdup (bmk->album);
    
    if (stored_artist)
    {
      g_free (stored_artist);
    }
    stored_artist = g_strdup (bmk->artist);
    
    if (current_filename)
    {
      g_free (current_filename);
    }
    current_filename     = g_strdup (bmk->filename);
    
    if (current_directory)
    {
      g_free (current_directory);
    }
    current_directory    = g_strdup (bmk->directory);
    
    display_track_info (TRUE);
  }
  else
  {
    stored_title        = NULL;
    stored_album        = NULL;
    stored_artist       = NULL;
  }
  
  if (current_filename)
  {
    keep_paused = config_paused;
    gssmp_ui_play_file (current_filename, TRUE, FALSE, current_time, TRUE);
    if (keep_paused)
    {
      gssmp_ui_pause_clicked ();
    }
  }
  
  redisplay_bookmarks ();
}

void choose_repeat_menu_item ()
{
  switch (config_repeat_style)
  {
    case no_repeat:
    {
      gtk_check_menu_item_set_active (no_repeat1,  TRUE);
      gtk_check_menu_item_set_active (repeat_all1, FALSE);
      gtk_check_menu_item_set_active (repeat_one1, FALSE);
      gtk_check_menu_item_set_active (shuffle1,    FALSE);
      break;
    }
    case repeat_all:
    {
      gtk_check_menu_item_set_active (no_repeat1,  FALSE);
      gtk_check_menu_item_set_active (repeat_all1, TRUE);
      gtk_check_menu_item_set_active (repeat_one1, FALSE);
      gtk_check_menu_item_set_active (shuffle1,    FALSE);
      break;
    }
    case repeat_one:
    {
      gtk_check_menu_item_set_active (no_repeat1,  FALSE);
      gtk_check_menu_item_set_active (repeat_all1, FALSE);
      gtk_check_menu_item_set_active (repeat_one1, TRUE);
      gtk_check_menu_item_set_active (shuffle1,    FALSE);
      break;
    }
    case shuffle:
    {
      gtk_check_menu_item_set_active (no_repeat1,  FALSE);
      gtk_check_menu_item_set_active (repeat_all1, FALSE);
      gtk_check_menu_item_set_active (repeat_one1, FALSE);
      gtk_check_menu_item_set_active (shuffle1,    TRUE);
      break;
    }
  }
}

void redisplay_bookmarks ()
{
  GtkMenu *submenu;
  
  gtk_widget_set_sensitive (GTK_WIDGET (recent1), FALSE);
  
  gtk_menu_item_deselect (GTK_MENU_ITEM (recent1));
  
  submenu = GTK_MENU (gtk_menu_new ());
  
  menu_number = 0;
  gssmp_config_bookmarks_foreach (menu_bookmark_callback_create, submenu);
  
  gtk_widget_show (GTK_WIDGET (submenu));
  
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (recent1), GTK_WIDGET (submenu));
  
  gtk_widget_set_sensitive (GTK_WIDGET (recent1), TRUE);
}

void menu_bookmark_callback_create (GssmpBookmark* bmk, gpointer submenu)
{
  GtkWidget *item;
  gchar* item_name;
  gchar* time_str;
  
  time_str = secs_to_time_str (bmk->time);
  
  item_name =  g_strdup_printf (_("%s - %s (%s)"), bmk->artist, bmk->track,
    time_str);
  item = gtk_menu_item_new_with_label (item_name);
  gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item);
  gtk_widget_show (GTK_WIDGET (item));
  
  g_signal_connect ((gpointer) item, "activate",
                    G_CALLBACK (gssmp_ui_choose_bookmark_clicked),
                    (gpointer)menu_number);
  
  ++menu_number;
  
  g_free (item_name);
  g_free (time_str);
}

gboolean status_callback (gpointer detailsptr)
{
  struct StatusDetails* details;
  
  status_timeout_source = 0;
  
  details = (struct StatusDetails*)detailsptr;
  
  gssmp_ui_display_status_message (details->context_id, details->msg,
    details->sub);
  
  g_slice_free (struct StatusDetails, details);
  
  return FALSE;
}

void play_in_dir (const gchar* dirname)
{
  gchar* first_file_name;
  
  gssmp_config_set_directory (dirname);
  
  if (config_repeat_style == shuffle)
  {
    first_file_name = gssmp_files_find_next_shuffled_file ();
  }
  else
  {
    first_file_name = gssmp_files_find_next_file_in_dir (
      current_directory, NULL);
  }

  if (first_file_name)
  {
    add_bookmark_here (); // Remember what we were playing
    gssmp_ui_play_file (first_file_name, FALSE, TRUE, 0, TRUE);
  }
}

void lastfm_enable_buttons ()
{
  const gchar* exe;
  gchar* fullexe;
  
  exe = gtk_entry_get_text (entry1);
  
  fullexe = g_find_program_in_path (exe);
  
  if (fullexe)
  {
    gtk_widget_set_sensitive (GTK_WIDGET (checkbutton1), TRUE);
    g_free (fullexe);
  }
  else
  {
    config_lastfm_active = FALSE;
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbutton1), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (checkbutton1), FALSE);
  }
}

